DROP TABLE IF EXISTS personnage;
DROP TABLE IF EXISTS joueur;
DROP TABLE IF EXISTS tables;
DROP TABLE IF EXISTS table_joueur;

CREATE TABLE joueur(
    j_id SERIAL NOT NULL PRIMARY KEY, 
    j_nom VARCHAR(30),
    j_prenom VARCHAR(30),
    j_pseudo VARCHAR(30),
    j_login VARCHAR(30) NOT NULL UNIQUE,
    j_pwd VARCHAR(30) NOT NULL
);

CREATE TABLE personnage(
    p_id SERIAL NOT NULL PRIMARY KEY,
    p_nom VARCHAR(30),
    j_id INTEGER,
    CONSTRAINT Foreign_perso FOREIGN KEY (j_id) REFERENCES joueur(j_id)
);

CREATE TABLE tables(
    t_id SERIAL NOT NULL PRIMARY KEY,
    t_id_mj INTEGER NOT NULL,
    t_systeme VARCHAR(30),
    t_date DATE
);

CREATE TABLE table_joueur(
    link_id_joueur INTEGER,
    link_id_table INTEGER NOT NULL
);

-- Fill table joueur
INSERT INTO joueur
VALUES (DEFAULT,'CAO','Thien An','Armorial','cao','anan00');
INSERT INTO joueur
VALUES (DEFAULT,'KYOUKO','Sakura','Sakyo','kyouko','puripuri');
INSERT INTO joueur
VALUES (DEFAULT,'TOMOE','Mami','Yellow','tomoe','jaune');
INSERT INTO joueur
VALUES (DEFAULT,'MIKI','Sayaka','Oktavia','miki','heuheu');
INSERT INTO joueur
VALUES (DEFAULT,'HOMURA','Akemi','Devil','homura','666');
INSERT INTO joueur
VALUES (DEFAULT,'KANAME','Madoka','God','kaname','magia');

-- Fill table personnage

INSERT INTO personnage
VALUES (DEFAULT,'Redhead',03);
INSERT INTO personnage
VALUES (DEFAULT,'Sunny',06);
INSERT INTO personnage
VALUES (DEFAULT,'Water',02);
INSERT INTO personnage
VALUES (DEFAULT,'Blackhole',05);
INSERT INTO personnage
VALUES (DEFAULT,'PinkyPie',02);
INSERT INTO personnage
VALUES (DEFAULT,'Merry',06);