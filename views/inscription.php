<div class="row">
    <div class="col-md-4 offset-md-4 card card-body">
        <form name="signup_form" action='controller/createUser.php' onsubmit="return equalsPassword()" method="post">
            <h3> Inscription </h3>

            <br>

            <?php if (!empty($_GET['err'])) { ?>
                    <div class="alert alert-danger" role="alert"> 
                        <strong>Erreur !</strong> <?php echo $_GET['err'] ?> 
                    </div>
                <?php } ?>

            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <input id="u_nom" class="form-control" name="u_nom" placeholder="Nom" type="text" required>
                    </div>
                    <div class="col">
                        <input id="u_prenom" class="form-control" name="u_prenom" placeholder="Prénom" type="text" required>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <input id="pseudo" class="form-control" name="pseudo" placeholder="Pseudo" type="text" required>
            </div>

            <div class="form-group">
                <input id="username" class="form-control" name="username" placeholder="Identifiant" type="text" required>
            </div>
            <div class="form-group">
                <input id="password" class="form-control" name="password" placeholder="Password" type="password"required minlength="6">
            </div>

            <div class="form-group">
                <input id="confirm_password" class="form-control" name="confirm_password" placeholder="Confirm Password" type="password"required>
            </div>
                
            <br>
            <button class="btn btn-default" type="submit">Finish</button>

            <br>
        </form>

        <br>
        <div class="text-center">
            Tu as déjà un compte ?
            <a href="login.php"> Connecte</a>-toi !
        </div>

    </div>
</div>

<script>
function equalsPassword() {
    var password = document.forms["signup_form"]["password"].value;
    var confirm_password = document.forms["signup_form"]["confirm_password"].value;

    if (password !== confirm_password) {
        alert("Passwords doesn't match");
        return false;
    }

    if (password.length < 6) {
        alert("Password too short !")
        return false;
    }
}
</script>