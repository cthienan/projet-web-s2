<div class="fiche_personnage">
    <?php
    foreach($data as $perso):?>
    <div>
        Nom du personnage: <?php echo $perso->getNameChar(); ?>
    </div>
    <div>
        ID Personnage: <?php echo $perso->getIdChar(); ?>
    </div>
    <div>
        Nom du joueur : <?php echo $perso->getLastNamePlayer(); ?> <?php echo $perso->getFirstNamePlayer(); ?>
    </div>
    <div>
        Pseudo du joueur : <?php echo $perso->getPseudo(); ?>
    </div>
    <?php if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] == true){ ?>
    <div>
        <a class="btn btn-default" href="./deletePerso.php?id=<?php echo $perso->getIdChar(); ?>" type="button">Supprimer</a>
    </div>
    <?php } 
    endforeach; ?>
</div>
