<?php if (!empty($_GET['msg'])) { ?>
    <div class="alert alert-success" role="alert"> 
        <div class="text-center"><strong>Succès !</strong> <?php echo $_GET['msg'] ?> </div>
    </div>
<?php } ?>

<div class="row">
    <div class="col-md-4 offset-md-4 card card-body">
        <form action='controller/authentif.php' method="post">
            <h3> Login </h3>

            <?php if (!empty($_GET['err'])) { ?>
                    <div class="alert alert-danger" role="alert"> 
                        <strong>Erreur !</strong> <?php echo $_GET['err'] ?> 
                    </div>
                <?php } ?>

            <div class="form-group">
                <label for="username">Identifiant</label>
                <input id="username" class="form-control" name="username" placeholder="Enter username" type="text" required>
            </div>
            <div class="form-group">
                <label for="password">Mot de Passe</label>
                <input id="password" class="form-control" name="password" placeholder="Enter password" type="password"required>
            </div>
                
            <br>
            <button class="btn btn-default" type="submit">Login</button>

            <br>
            <br>
            <div class="text-center">
                Pas encore de compte ?
                <a href="login.php"> Inscris</a>-toi !
            </div>
        </form>
    </div>
</div>