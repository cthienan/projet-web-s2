<header>
  <h1> Bienvenue sur RollTwentIIE</h1>
</header>
<nav class="navbar navbar-expand-sm" role="navigation">
  <ul class="nav navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href=index.php>Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href=getperso.php>Personnages</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href=about.php>About</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href=calendar.php>Calendrier</a>
      </li>
  </ul>
    <?php if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){ ?>
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href=login.php>Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=signup.php>Sign Up</a>
      </li>
    </ul>
    <?php } 
      else { ?>
    <ul class="nav navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href=secret.php>Secret</a>
      </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href=getjoueur.php>Profil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href=logout.php>Logout</a>
      </li>
    </ul>
    <?php } ?>
  </ul>
</nav>
<br>