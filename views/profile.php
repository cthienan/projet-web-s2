<!-- nom - prénom - pseudo - modifier identifiant - modifier le mdp - supprimer le compte-->

<div>
<?php
    foreach($data as $joueur):?>
    <div>
        ID du joueur: <?php echo $joueur->getID(); ?>
    </div>
    <div>
        Prénom du joueur: <?php echo $joueur->getFirstName(); ?>
    </div>
    <div>
        Pseudo du joueur: <?php echo $joueur->getPseudo(); ?>
    </div>
    <div>
        Nom du joueur : <?php echo $joueur->getLastName(); ?>
    </div>
    <?php if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] == true){ ?>
    <div>
        <a class="btn btn-default" href="./deleteJoueur.php?id=<?php echo $joueur->getId(); ?>" type="button">Supprimer</a>
    </div>
    <?php } 
    endforeach; ?>
</div>
