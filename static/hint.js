function showHint(str) {
    var xhttp;
    if (str.length == 0) { 
        document.getElementById("nameHint").innerHTML = "-"; //en cas de chaîne vide, la suggestion affiche un simple "-"
    return;
 }
xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
 if (this.readyState == 4 && this.status == 200) {// 4 = request finished and response is ready, 200 = "OK"
     document.getElementById("nameHint").innerHTML = this.responseText;
    }
};
 xhttp.open("GET", 'controller/gethint.php?q='+str, true); //on appelle gethint.php avec l'élément que l'on a récupéré
xhttp.send();   
}