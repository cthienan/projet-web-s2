<?php

session_start();
 
$_SESSION = array();
 
//On détruit la session en cours
session_destroy();

//On redirige vers la page login
header("location: login.php");
exit;
?>