<?php

$u_nom = trim($_POST['u_nom']); 
$u_prenom = trim($_POST['u_prenom']);

$pseudo = trim($_POST['pseudo']); 
$username = trim($_POST['username']); 
$password = trim($_POST['password']);
$confirm_password = trim($_POST['confirm_password']);

$err='';

include('dbplayer.php');

$db = connect(); //connexion à la base de donnée
$st = $db->prepare("SELECT j_login FROM joueur WHERE j_login = :username");
$st->bindValue(':username', $username, \PDO::PARAM_INT);
$st->execute();

$res = $st->fetchAll();

if (!empty($res)) {
    $err = 'Identifiant déjà pris. Veuillez en choisir un autre.';
    header("location: ../signup.php?err=$err");
}

$st = $db->prepare("INSERT INTO joueur (j_id,j_nom,j_prenom, j_pseudo, j_login, j_pwd) VALUES (DEFAULT,:nom,:prenom,:pseudo,:username,:pass)");
$st->bindValue(':nom', $u_nom, \PDO::PARAM_INT);
$st->bindValue(':prenom', $u_prenom, \PDO::PARAM_INT);
$st->bindValue(':pseudo', $pseudo, \PDO::PARAM_INT);
$st->bindValue(':username', $username, \PDO::PARAM_INT);
$st->bindValue(':pass', $password, \PDO::PARAM_INT);
$st->execute();

$suc = "Votre compte a bien été créé ! Vous pouvez maintenant vous connecter.";

header("location: ../login.php?msg=$suc");

?>