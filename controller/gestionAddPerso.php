<?php

include_once('dbplayer.php');
include_once('searchFunction.php');

function addPersonnage(string $name, int $id) {

    //utiliser MAX(p_id) pour l'insert

    $db=connect();
    $st = $db->prepare("INSERT INTO personnage (p_id,p_nom,j_id) VALUES (DEFAULT,:name,:id)");
    $st->bindParam(':name', $name);
    $st->bindParam(':id', $id);
    $st->execute();

}

function deletePersonnage(int $id) {

    $db=connect();
    $st = $db->prepare("DELETE FROM personnage WHERE p_id = :id");
    $st->bindParam(':id',$id);
    $st->execute();
}

?>