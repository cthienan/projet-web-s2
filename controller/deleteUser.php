<?php

include_once('dbplayer.php');
include_once('searchFunction.php');

function deleteJoueur(int $id) {

    $db=connect();
    
    // On supprime les persos du joueur
    $st = $db->prepare("DELETE FROM personnage WHERE j_id = :id");
    $st->bindParam(':id',$id);
    $st->execute();

    // Puis on supprime le joueur
    $st = $db->prepare("DELETE FROM joueur WHERE j_id = :id");
    $st->bindParam(':id',$id);
    $st->execute();
}

?>