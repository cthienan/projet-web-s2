<?php

include('./models/Character.php');
include('./models/Player.php');
include_once('dbplayer.php');

//fonction hydrate
//prend en paramètre une ligne de la base de donnée et renvoie un élément de la classe personnage avec les paramètres adaptés

function hydrate($data) {

  $character = new Character();
  $character->setIdChar($data['p_id']);
  $character->setNameChar($data['p_nom']);
  $character->setLastNamePlayer($data['j_nom']);
  $character->setFirstNamePlayer($data['j_prenom']);
  $character->setPseudo($data['j_pseudo']);
  return $character;
}

//fonction findPersoByName
//prend en paramètre un string nom et renvoie des données de classe personnage associées

function findPersoByName(string $name) {

  $fiche = [];

  //On récupère les données de la BDD associées au nom $name sous forne d'un tableau

  $db = connect();
  $st = $db->prepare("SELECT * FROM personnage NATURAL JOIN joueur WHERE p_nom = :name");
  $st->bindValue(':name', $name, \PDO::PARAM_INT);
  $st->execute();

  //on convertit le tableau de données en classe personnage

  foreach ($st->fetchAll() as $raw) {
    $fiche[] = hydrate($raw);
  }

  //on renvoie

  return $fiche;
}

//Fonctionnement identique à FindPersoByName, mais fonctionne avec l'id du personnage

function findPersoById(int $id) {

  $fiche = [];

  $db = connect();
  $st = $db->prepare("SELECT * FROM personnage NATURAL JOIN joueur WHERE p_id = :id");
  $st->bindValue(':id', $id, \PDO::PARAM_INT);
  $st->execute();

  foreach ($st->fetchAll() as $raw) {
    $fiche[] = hydrate($raw);
  }

  return $fiche;
}

//fonction qui renvoie la liste de tous les personnages, de façon identique aux fonctions précédentes

function listPerso() {

  $fiche = [];

  $db = connect();
  $st = $db->prepare("SELECT * FROM personnage NATURAL JOIN joueur");
  $st->execute();

  foreach ($st->fetchAll() as $raw) {
    $fiche[] = hydrate($raw);
  }

  return $fiche;
}

//fonction hydrate
//prend en paramètre une ligne de la base de donnée et renvoie un élément de la classe joueur avec les paramètres adaptés

function hydratePlayer($data) {

  $player = new Player();
  $player->setId($data['j_id']);
  $player->setLastName($data['j_nom']);
  $player->setFirstName($data['j_prenom']);
  $player->setPseudo($data['j_pseudo']);
  return $player;
}

//Fonctionnement identique à findPersoById, mais fonctionne avec l'id du joueur

function findPLayerByID(int $id) {

  $profile = [];

  $db = connect();
  $st = $db->prepare("SELECT * FROM joueur WHERE j_id = :id");
  $st->bindValue(':id', $id, \PDO::PARAM_INT);
  $st->execute();

  foreach ($st->fetchAll() as $raw) {
    $profile[] = hydratePlayer($raw);
  }

  return $profile;
}

?>