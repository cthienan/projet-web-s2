<?php

$username = trim($_POST['username']); 
$password = trim($_POST['password']); 
$err = '';

// Si aucune erreur a été détecté, on fait une requête demandant à la base de donnée l'identifiant et le mot de passe correspondant
if (empty($error)) {
    include('dbplayer.php');

    $db = connect(); //connexion à la base de donnée
    $st = $db->prepare("SELECT j_id, j_login, j_pwd FROM joueur WHERE j_login = :username AND j_pwd = :pwd");
    $st->bindValue(':username', $username, \PDO::PARAM_INT);
    $st->bindValue(':pwd', $password, \PDO::PARAM_INT);
    $st->execute();

    $res = $st->fetchAll();

    $st = null;

    // Si aucun couple identifant/mdp n'est trouvé dans la base, on envoie une erreur idiquant que le couple n'est pas valide
    if(empty($res)) {
        $err = 'Identifiant ou mot de passe invalide';
        header("location: ../login.php?err=$err");
    }
    else {
        // On crée une nouvelle session qui indique que la personne est connecté et on enregistre l'id et le login de l'utilisateur
        session_start();

        $_SESSION['loggedin'] = true;
        $_SESSION['id'] = $res[0]['j_id'];
        $_SESSION['username'] = $res[0]['j_login'];

        header('location: ../index.php');
    }
}

?>