<?php
include("config.php");

// On crée une fonction permettant de se connecter à la base de donnée à l'aide du fichier config.php qui contient les différentes infos
// our la connexion. 
function connect() {
	global $HOST, $DSTB, $USER, $PASS, $PORT;
	return new \PDO("pgsql:dbname=$DSTB;host=$HOST;port=$PORT", $USER, $PASS);
}

?>