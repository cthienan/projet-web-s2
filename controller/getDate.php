<?php

include_once('dbplayer.php');
include('./models/Tables.php');

function hydrate($data) {

    $table = new Table();
    $table->setIdTable($data['t_id']);
    $table->setIdMJ($data['t_id_mj']);
    $table->setSysteme($data['t_systeme']);
    $table->setDate($data['t_date']);
    return $table;
  }

function allDate() {
    
    $data = [];

  $db = connect();
  $st = $db->prepare("SELECT * FROM tables");
  $st->execute();

  foreach ($st->fetchAll() as $raw) {
    $data[] = hydrate($raw);
  }

  return $data;
}


function verifDate($date,$table) {

    $d = $table->getDate();
    $id = $table->getIdTable();

    if ($d == $date) {
        return "Table ce jour : $id";
    } 
    else{
       return "Pas de table ce jour";
    }
}


?>