# RollTwentIIE

Ce site a été créé pour le mini-projet web de S2. 

Liste des éléments à ajouter ou à améliorer pour le gros projet web : 
- Un système d'inscription
- Un formulaire de création de personnage
- Amélioration de la validation des formulaire avec Javascript
- Amélioration de la table personnage pour y ajouter plus d'informations
- Ajout de nouvelles tables
- (Optionnel) Ajout d'un lanceur de dés
- (Optionnel) Ajout d'un calendrier pour lister les prochaines parties prévues

cc