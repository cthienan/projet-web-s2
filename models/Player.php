<?php

class Player {

    private $idPlayer;
    private $FirstNamePlayer;
    private $LastNamePlayer;
    private $PseudoPlayer;

    public function getId () {
        return $this->idPlayer;
    }

    public function setId ($id) {
        $this->idPlayer = $id;
        return $this->idPlayer;
    }

    public function getFirstName () {
        return $this->FirstNamePlayer;
    }

    public function setFirstName ($name) {
        $this->FirstNamePlayer = $name;
        return $this->FirstNamePlayer;
    }

    public function getLastName () {
        return $this->LastNamePlayer;
    }

    public function setLastName ($name) {
        $this->LastNamePlayer = $name;
        return $this->LastNamePlayer;
    }

    public function getPseudo () {
        return $this->PseudoPlayer;
    }

    public function setPseudo ($pseudo) {
        $this->PseudoPlayer = $pseudo;
        return $this->PseudoPlayer;
    }

}

?>