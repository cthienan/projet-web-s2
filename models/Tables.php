<?php

class Table {
    private $idTable;
    private $idMJ;
    private $systeme;
    private $date;


    public function getIdTable() {
        return $this->idTable;
    }

    public function getIdMJ() {
        return $this->idMJ;
    }

    public function getSysteme() {
        return $this->systeme;
    }

    public function getDate() {
        return $this->date;
    }

    public function setIdTable($id) {
        $this->idTable = $id;
        return $this->idTable;
    }

    public function setIdMJ($id) {
        $this->idMJ = $id;
        return $this->idMJ;
    }

    public function setSysteme($sys) {
        $this->systeme = $sys;
        return $this->systeme;
    }

    public function setDate($date) {
        $this->date = $date;
        return $this->date;
    }
}

?>