<?php

class Character {

    private $idChar;
    private $NameChar;

    private $idPlayer;
    private $LastNamePlayer;
    private $FirstNamePlayer;
    private $PseudoPlayer;

    public function getIdChar () {
        return $this->idChar;
    }

    public function setIdChar ($id) {
        $this->idChar = $id;
        return $this->idChar;
    }

    public function getNameChar () {
        return $this->NameChar;
    }

    public function setNameChar ($name) {
        $this->NameChar = $name;
        return $this->idChar;
    }

    public function getIdPlayer () {
        return $this->idPlayer;
    }

    public function setIdPlayer ($id) {
        $this->idPlayer = $id;
        return $this->idPlayer;
    }

    public function getFirstNamePlayer () {
        return $this->FirstNamePlayer;
    }

    public function setFirstNamePlayer ($name) {
        $this->FirstNamePlayer = $name;
        return $this->FirstNamePlayer;
    }

    public function getLastNamePlayer () {
        return $this->LastNamePlayer;
    }

    public function setLastNamePlayer ($name) {
        $this->LastNamePlayer = $name;
        return $this->LastNamePlayer;
    }

    public function getPseudo () {
        return $this->PseudoPlayer;
    }

    public function setPseudo ($pseudo) {
        $this->PseudoPlayer = $pseudo;
        return $this->PseudoPlayer;
    }

}

?>