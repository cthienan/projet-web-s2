<?php

session_start();

include_once('controller/searchFunction.php');

//On regarde d'abord si la clé j_id existe dans le dictionnaire POST, et si c'est le cas, cela veut dire qu'on crée un perso
if(array_key_exists('j_id', $_POST)) {
    $id = (int) $_POST['j_id'];
    $name = $_POST['personame'];

    include_once('controller/gestionAddPerso.php');
    addPersonnage($name,$id);

    $data = findPersoByName($name);
}

if(array_key_exists('recherche', $_POST)) {
    $name = $_POST['recherche']; //le nom du personnage est ce qui est envoyé par le formulaire
    $data = findPersoByName($name); //on récupère les données du personnage en question
}

if(array_key_exists('id', $_GET)) {
    $id = $_GET['id']; //on récupère l'id qui a été entrée dans le lien url
    $data = findPersoById($id); // on cherche le personnage ayant cet id dans la base de donnée
}

// On charge la vue correspondante
include('views/template.php'); 
loadView('perso',$data);
?>